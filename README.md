# Max Flow Min Cut Theorem in Lean 4

This is a Lean 4 version of [Aleksandar Milchev, Leo Okawa Ericson, Viggo Laakshoharju's lean3 proof](https://github.com/leanprover-community/mathlib/blob/max_flow_min_cut/src/combinatorics/quiver/max_flow_min_cut.lean). I have made changes in many definitions to make proving things easier for me. The result is shorter proofs than the lean 3 version.

Update : Defining a separate structure for Cuts seems unnecessary. The corresponding lemmas are mentioned in a comment if I see them.