import Lake
open Lake DSL

package «maxFlowMinCut» {
  -- add any package configuration options here
}

require mathlib from git
  "https://github.com/leanprover-community/mathlib4.git"

@[default_target]
lean_lib «MaxFlowMinCut» {
  -- add any library configuration options here
}
