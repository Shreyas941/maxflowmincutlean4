import Mathlib

/-
This is a Lean 4 version of Aleksandar Milchev, Leo Okawa Ericson, Viggo Laakshoharju's lean3 proof. 
I (Shreyas) have made changes in many definitions to make proving things easier for me. 
The result is shorter proofs than the lean 3 version.
-/

open BigOperators
open Classical

universe u

variable (V : Type u) [Fintype V] [DecidableEq V]

structure Digraph where
  isEdge : V → V → Prop
  isNonSymm : ∀ u v : V, ¬((isEdge u v) ∧ (isEdge u v))
  

noncomputable def outNeighbours {V : Type u} [Fintype V] 
  (G : Digraph V) (v : V): Finset V := 
  {w ∈ Finset.univ | G.isEdge v w}.toFinset

noncomputable def inNeighbours {V : Type u} [Fintype V] 
  (G : Digraph V) (v : V) : Finset V :=
  {w ∈ Finset.univ | G.isEdge w v}.toFinset

noncomputable def outDegree {V : Type u} [Fintype V] 
  (G : Digraph V) (v : V) := 
  (outNeighbours G v).card

noncomputable def addsToOutDegree {V : Type u} [Fintype V] 
  (G : Digraph V) (w : V) (v : V) := G.isEdge w v

noncomputable def addsToInDegree {V : Type u} [Fintype V] 
  (G : Digraph V) (w : V) (v : V) := G.isEdge v w
  

noncomputable def inDegree {V : Type u} [Fintype V] 
  (G : Digraph V) (v : V) := 
  (inNeighbours G v).card


noncomputable def totalOutDegree {V : Type u} [Fintype V] 
  (G : Digraph V) := ∑ v in Finset.univ, outDegree G v

noncomputable def totalOutDegree_alt {V : Type u} [Fintype V] 
  (G : Digraph V) := ∑ v in Finset.univ, ∑ w in Finset.univ, if G.isEdge v w then 1 else 0


noncomputable def totalInDegree {V : Type u} [Fintype V] 
  (G : Digraph V) := ∑ v in Finset.univ, inDegree G v

noncomputable def totalInDegree_alt {V : Type u} [Fintype V]
  (G : Digraph V) := ∑ v in Finset.univ, ∑ w in Finset.univ, if G.isEdge w v then 1 else 0


theorem inDegAlt_Eq_inDeg {V : Type u} [Fintype V] :
  ∀ G : Digraph V, totalInDegree G = totalInDegree_alt G := by
  intro G
  unfold totalInDegree totalInDegree_alt
  unfold inDegree inNeighbours
  simp only [Finset.mem_univ, 
    true_and, 
    Set.toFinset_setOf, 
    forall_true_left, 
    Finset.sum_boole, 
    Nat.cast_id]

theorem outDegAlt_Eq_outDeg {V : Type u} [Fintype V] :
  ∀ G : Digraph V, totalOutDegree G = totalOutDegree_alt G := by
  intro G
  unfold totalOutDegree totalOutDegree_alt
  unfold outDegree outNeighbours
  simp only [Finset.mem_univ, 
    true_and, 
    Set.toFinset_setOf, 
    forall_true_left, 
    Finset.sum_boole, 
    Nat.cast_id]
  

theorem inDeg_Equals_outDeg_alt {V : Type u} [Fintype V]: 
  ∀ G : Digraph V, totalInDegree_alt G = totalOutDegree_alt G := by
  intro G
  unfold totalInDegree_alt totalOutDegree_alt
  exact Finset.sum_comm

theorem inDeg_Equals_outDeg {V : Type u} [Fintype V]: 
  ∀ G : Digraph V, totalInDegree G = totalOutDegree G := by
  intro G
  rw[inDegAlt_Eq_inDeg, outDegAlt_Eq_outDeg]
  apply inDeg_Equals_outDeg_alt


structure CapacityDiGraph (V : Type u) [Fintype V] 
  extends Digraph V where
  capacity : V → V → ℝ
  nonEdgeZeroCap : ∀ u v : V, ¬isEdge u v → capacity u v = 0
  nonNegCapacity : ∀ u v : V, capacity u v ≥ 0 

structure FlowNetwork (V : Type u) [Fintype V] 
  extends CapacityDiGraph V where
  source : V
  sink : V

noncomputable def flow_in {V : Type u} [Fintype V] [DecidableEq V] 
  (flow : V → V → ℝ) 
  (vSet : Finset V)
  : ℝ := 
    ∑ s in (Finset.univ \ vSet),  ∑ t in vSet, flow s t

noncomputable def flow_out {V : Type u} [Fintype V] [DecidableEq V]
  (flow : V → V → ℝ)
  (vSet : Finset V)
  : ℝ :=
    ∑ s in vSet, (∑ t in (Finset.univ \ vSet), flow s t)

noncomputable def flow_from_to {V : Type u} [Fintype V] [DecidableEq V]
  (flow : V → V → ℝ)
  (S T : Finset V)
  : ℝ :=
     ∑ x in S,  ∑ y in T, flow x y

noncomputable def flow_net {V : Type u} [Fintype V] [DecidableEq V]
  (flow : V → V → ℝ)
  (vSet : Finset V)
  :ℝ :=
    flow_out flow vSet - flow_in flow vSet 




structure GraphWithFlow (V : Type u) [Fintype V] [DecidableEq V]
  where
    graph : FlowNetwork V
    flow : V → V → ℝ
    flow_non_neg : ∀ v w : V, flow v w ≥ 0
    flow_bound_capacity : ∀ v w : V, flow v w ≤ graph.capacity v w
    source_neq_sink : graph.source ≠ graph.sink
    source_no_in : ∀ v, ¬ graph.isEdge v graph.source 
    sink_no_out : ∀ v, ¬ graph.isEdge graph.sink v
    flow_conservation : ∀ v : V, v ≠ graph.source ∧ v ≠ graph.sink → 
        flow_out flow {v} = flow_in flow {v}
    -- The cut does not have to be a separate structure.
    cut : Finset V
    source_in_cut : graph.source ∈ cut
    sink_not_in_cut : graph.sink ∉ cut

noncomputable def FlowValue {V : Type u} [Fintype V]
  (G : GraphWithFlow V) : ℝ :=
    flow_out G.flow {G.graph.source} - flow_in G.flow {G.graph.source}
  
noncomputable def cutEdge {V : Type u} [Fintype V]
  (G : GraphWithFlow V) (v w : V): Prop := G.graph.isEdge v w ∧ v ∈ G.cut ∧ w ∉ G.cut

noncomputable def cutFlowOut {V : Type u} [Fintype V] [DecidableEq V]
  (G : GraphWithFlow V) := flow_out G.flow G.cut

noncomputable def cutFlowIn {V : Type u} [Fintype V] [DecidableEq V]
  (G : GraphWithFlow V) := flow_in G.flow G.cut

noncomputable def cutCapacity {V : Type u} [Fintype V] [DecidableEq V]
  (G : GraphWithFlow V) := flow_out G.graph.capacity G.cut

theorem no_edge_zero_flow {V : Type u} [Fintype V]:
  ∀ (G : GraphWithFlow V), ∀ v w : V, ¬ G.graph.isEdge v w → G.flow v w = 0 := by
  intro G
  intro v w
  intro nonEdge
  have capacity_zero : G.graph.capacity v w = 0 := by
    apply G.graph.nonEdgeZeroCap
    exact nonEdge
  have flow_leq_zero : G.flow v w ≤ 0 := by
    apply le_trans
    apply G.flow_bound_capacity
    apply le_of_eq
    apply capacity_zero
  have flow_geq_zero : G.flow v w ≥ 0 := by
    apply G.flow_non_neg
  linarith [flow_leq_zero, flow_geq_zero]

theorem flow_self {V : Type u} [Fintype V] :
  ∀ (G : GraphWithFlow V), ∀ v : V, G.flow v v = 0 := by
  intro G v
  apply no_edge_zero_flow
  have no_self_edge : ¬ (G.graph.isEdge v v ∧ G.graph.isEdge v v) := by
    apply (G.graph.isNonSymm v v)
  tauto

theorem flow_in_single_node {V : Type u} [Fintype V]:
  ∀ (G : GraphWithFlow V), ∀ (x : V), flow_in G.flow {x} = ∑ v in Finset.univ, G.flow v x := by 
  intro G x
  unfold flow_in
  simp only [Finset.mem_univ, not_true, Finset.sum_singleton, Finset.subset_univ, Finset.sum_sdiff_eq_sub,
  sub_eq_self]
  apply flow_self

theorem flow_out_single_node {V : Type u} [Fintype V]:
  ∀ (G : GraphWithFlow V), ∀ (x : V), flow_out G.flow {x} = ∑ v in Finset.univ, G.flow x v := by
  intro G x
  unfold flow_out
  simp only [Finset.mem_univ, not_true, Finset.subset_univ, Finset.sum_sdiff_eq_sub, Finset.sum_singleton,
  Finset.sum_sub_distrib, sub_eq_self]
  apply flow_self

noncomputable def netVertexFlow {V : Type y} [Fintype V]
  (G : GraphWithFlow V) (x : V) : ℝ := flow_out G.flow {x} - flow_in G.flow {x}

theorem net_flow_zero (V : Type u) [Fintype V] (G : GraphWithFlow V) (x : V) : 
  x ≠ G.graph.source → x ≠ G.graph.sink → netVertexFlow G x = 0 := by
  intro x_neq_source x_neq_sink
  unfold netVertexFlow
  apply sub_eq_zero.mpr
  apply G.flow_conservation
  apply And.intro
  exact x_neq_source
  exact x_neq_sink

theorem set_flow_conservation {V : Type u} [Fintype V] 
  (G : GraphWithFlow V) (S : Finset V):
  G.graph.source ∉ S ∧  G.graph.sink ∉ S  → flow_in G.flow S = flow_out G.flow S := by
  intro S_subset_rules
  cases' S_subset_rules with source_cond sink_cond
  unfold flow_in flow_out
  simp only [Finset.subset_univ, Finset.sum_sdiff_eq_sub, Finset.sum_sub_distrib, sub_left_inj]
  rw [Finset.sum_comm, Finset.sum_congr]
  rfl
  intro x x_in_S
  have x_neq_source : x ≠ G.graph.source := by
    by_contra x_eq_source
    rw [←x_eq_source] at source_cond
    exact (source_cond x_in_S)
  have x_neq_sink : x ≠ G.graph.sink := by
    by_contra x_eq_sink
    rw [←x_eq_sink] at sink_cond
    exact (sink_cond x_in_S)
  have flow_cons := G.flow_conservation x (And.intro x_neq_source x_neq_sink)
  rw [flow_in_single_node G x] at flow_cons
  rw [flow_out_single_node G x] at flow_cons
  apply flow_cons.symm

 
theorem flow_out_split {V : Type u} [Fintype V] [DecidableEq V]
  (flow :V → V → ℝ)
  (S T : Finset V)
  (disjoint : S ∩ T = ∅)
  : flow_out flow S = flow_from_to flow S T + flow_from_to flow S ((Finset.univ \ S) \ T) := by
  unfold flow_out flow_from_to
  have disj : Finset.univ \ S = T ∪ ((Finset.univ \ S)\T) := by
    simp only [Finset.union_sdiff_self_eq_union, Finset.right_eq_union_iff_subset]
    -- Why does ext not work here like it does in lean 4. 
    sorry
  sorry

theorem flow_in_split {V : Type u} [Fintype V] [DecidableEq V]
  (flow :V → V → ℝ)
  (S T : Finset V)
  (disjoint : S ∩ T = ∅)
  : flow_in flow S = flow_from_to flow T S + flow_from_to flow ((Finset.univ \ S) \ T) S := by
  unfold flow_in flow_from_to
  have disj : Finset.univ \ S = T ∪ ((Finset.univ\ S)\T) := by
    simp only [Finset.union_sdiff_self_eq_union, Finset.right_eq_union_iff_subset]
    -- Why does ext not work here like it does in lean 4. 
    sorry
  sorry

theorem emptySet_flow_from {V : Type u} [Fintype V] [DecidableEq V]
  (flow : V → V → ℝ)
  (S : Finset V) : flow_from_to flow ∅ S = 0 := by
  unfold flow_from_to
  simp only [Finset.sum_empty, Finset.sum_const_zero]

theorem emptySet_flow_to {V : Type u} [Fintype V] [DecidableEq V]
  (flow : V → V → ℝ)
  (S : Finset V) : flow_from_to flow S ∅ = 0 := by
  unfold flow_from_to
  simp only [Finset.sum_empty, Finset.sum_const_zero]

theorem from_to_flow_disjoint₁ {V : Type u} [Fintype V] [DecidableEq V]
  (flow : V → V → ℝ)
  (S T U : Finset V) 
  (ST_disjoint : S ∩ T = ∅)
  (SU_disjoint : S ∩ U = ∅)
  (TU_disjoint : T ∩ U = ∅)
  : flow_from_to flow (S ∪ T) U = flow_from_to flow S U + flow_from_to flow T U := by
  library_search
  unfold flow_from_to
  sorry
#check Finset.sum_disjUnion
theorem from_to_flow_disjoint₂ {V : Type u} [Fintype V] [DecidableEq V]
  (flow : V → V → ℝ)
  (S T U : Finset V) 
  (ST_disjoint : S ∩ T = ∅)
  (SU_disjoint : S ∩ U = ∅)
  (TU_disjoint : T ∩ U = ∅)
  : flow_from_to flow U (S ∪ T) = flow_from_to flow U S + flow_from_to flow U T := by
  sorry

theorem netflow_disjoint_union {V : Type u} [Fintype V] [DecidableEq V]
  (G : GraphWithFlow V) (S T : Finset V) (disjoint : S ∩ T = ∅) : 
  flow_net G.flow (S ∪ T) = flow_net G.flow S + flow_net G.flow T := by
  unfold flow_net
  have basic₁ : (Finset.univ \ S)\T = (Finset.univ \ T) \ S := by
    ext x
    constructor
    case a.mp => 
      intro x_in
      simp only [Finset.mem_sdiff, Finset.mem_univ, true_and] at x_in
      simp only [Finset.mem_sdiff, Finset.mem_univ, true_and]
      tauto
    case a.mpr =>
      intro x_in
      simp only [Finset.mem_sdiff, Finset.mem_univ, true_and] at x_in
      simp only [Finset.mem_sdiff, Finset.mem_univ, true_and]
      tauto
  have basic₂ : (Finset.univ \ (S ∪ T)) = (Finset.univ \ S) \T := by
    ext x
    constructor
    case a.mp =>
      intro x_in
      simp only [Finset.mem_sdiff, Finset.mem_univ, Finset.mem_union, true_and] at x_in
      simp only [Finset.mem_sdiff, Finset.mem_univ, Finset.mem_union, true_and]
      tauto
    case a.mpr =>
      intro x_in;
      simp only [Finset.mem_sdiff, Finset.mem_univ, Finset.mem_union, true_and] at x_in
      simp only [Finset.mem_sdiff, Finset.mem_univ, Finset.mem_union, true_and]
      tauto
  rw [(flow_out_split G.flow S T), (flow_in_split G.flow S T)]
  rw [(flow_out_split G.flow T S), (flow_in_split G.flow T S)]
  rw [basic₁]
  ring_nf
  rw [(flow_out_split G.flow (S ∪ T) (Finset.univ \ (S ∪ T)))]
  rw [(flow_in_split G.flow (S ∪ T) (Finset.univ \ (S ∪ T)))]
  rw [basic₂, basic₁]
  simp
  rw [emptySet_flow_from, emptySet_flow_to]
  ring_nf
  rw [from_to_flow_disjoint₁, from_to_flow_disjoint₂]
  ring
  exact disjoint
  simp only [Finset.inter_sdiff_self]
  rw [←basic₁]
  simp only [Finset.inter_sdiff_self]
  exact disjoint
  simp only [Finset.inter_sdiff_self]
  rw [←basic₁]
  simp only [Finset.inter_sdiff_self]
  simp only [Finset.inter_sdiff_self]
  apply Finset.inter_sdiff_self
  rw [←(Finset.inter_comm S T)]
  exact disjoint
  rw [←(Finset.inter_comm S T)]
  exact disjoint
  exact disjoint
  exact disjoint 

-- flow_value_global_ver
theorem out_cut_flow_eq_out_source_flow {V : Type u} [Fintype V] [DecidableEq V]
  (G : GraphWithFlow V) 
  : flow_net G.flow G.cut = flow_net G.flow {G.graph.source}  := by  
  have split_cut : G.cut = {G.graph.source} ∪ (G.cut \ {G.graph.source}) := by
    simp only [Finset.union_sdiff_self_eq_union, Finset.right_eq_union_iff_subset, Finset.singleton_subset_iff]
    exact G.source_in_cut;
  rw [split_cut, netflow_disjoint_union]
  have source_not_in_second : G.graph.source ∉ (G.cut \ {G.graph.source}) := by
    simp only [Finset.mem_sdiff, Finset.mem_singleton, not_true, and_false, not_false_iff]
  have sink_not_in_second : G.graph.sink ∉ (G.cut \ {G.graph.source}) := by
    have sink_not_in_cut := G.sink_not_in_cut
    simp only [Finset.mem_sdiff, Finset.mem_singleton, not_and, not_not]
    contrapose
    intro
    exact sink_not_in_cut
  have non_source_total_flow_zero : flow_net G.flow (G.cut \ {G.graph.source}) = 0 := by
    unfold flow_net
    have conservation := 
      (set_flow_conservation G (G.cut \ {G.graph.source}) (And.intro source_not_in_second sink_not_in_second)).symm
    sorry

  sorry








